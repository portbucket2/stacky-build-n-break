﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bomb : MonoBehaviour
{
    readonly string PLAYER = "Player";
    readonly string NPC = "npc";

    [Header("Debug")]
    [SerializeField] GroundSpwaner spwaner;


    WaitForSeconds WAIT = new WaitForSeconds(1.5f);

    private void Awake()
    {
        
    }

    void Start()
    {
        spwaner = GetComponentInParent<GroundSpwaner>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(PLAYER))
        {
            other.GetComponent<Player>().AddBomb();
            ReSpwan();
        }
        else if (other.CompareTag(NPC))
        {
            other.GetComponent<NPC>().AddBomb();
            ReSpwan();
        }
        else
        {

        }
    }

    void ReSpwan()
    {
        StartCoroutine(ReSpwanRoutine());
    }
    IEnumerator ReSpwanRoutine()
    {
        GetComponent<SphereCollider>().enabled = false;
        transform.GetChild(0).gameObject.SetActive(false);
        yield return WAIT;
        spwaner.RandomSpwan(transform.position);
        gameObject.SetActive(false);
    }
}
