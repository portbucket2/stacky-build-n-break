﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    public static GameController gameController;

    private void Awake()
    {
        gameController = this;
    }
    public static GameController GetController() { return gameController; }

    [SerializeField] Player player;
    [SerializeField] NPC npc;
    [SerializeField] UIController UI;
    [SerializeField] GroundSpwaner spwaner;
    [SerializeField] ParticleSystem confetti;

    WaitForSeconds WAITONE = new WaitForSeconds(1.5f);

    AnalyticsController analytics;

    void Start()
    {
        analytics = AnalyticsController.GetController();
    }

    public void LevelStart()
    {

        npc.BehaviorStart();

        analytics = AnalyticsController.GetController();
        analytics.LevelStarted();
    }
    public void LevelComplete()
    {
        analytics.LevelEnded();
        StartCoroutine(LevelCompleteRoutine());
    }
    IEnumerator LevelCompleteRoutine()
    {

        confetti.Play();
        npc.StopAllActivity();
        player.InputSwitch(false);
        yield return WAITONE;
        UI.LevelCompleted();
        yield return WAITONE;
        player.Victory();

    }
    public void LevelFailed()
    {
        analytics.LevelFailed();
        UI.LevelFailed();
        npc.StopAllActivity();
        player.InputSwitch(false);
    }
}
