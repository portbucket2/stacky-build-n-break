﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundSpwaner : MonoBehaviour
{
    [SerializeField] Transform startTransform;
    [SerializeField] int rows = 5;
    [SerializeField] int columns = 5;
    [SerializeField] float distance = 3f;
    [SerializeField] GameObject brick;
    [SerializeField] GameObject bomb;

    void Start()
    {
        SetSpwaner();
    }


    void SetSpwaner()
    {
        Vector3 offset = new Vector3(1f, 0f, 1f);
        Vector3 start = startTransform.position;
        Vector3 pos = start;
        for (int i = 0; i < rows; i++)
        {
            for (int j = 0; j < columns; j++)
            {
                pos = start + new Vector3(j * distance, 0f, i * distance);
                //SpwanPlayerBrick(pos);
                RandomSpwan(pos);
            }
                
        }
    }
    public void RandomSpwan(Vector3 _pos)
    { 
        int rand = Random.Range(0, 100);
        if (rand < 10)
        {
            // bomb
            SpwanBomb(_pos);
        }
        else if (rand >10 && rand < 60)
        {
            // player brick
            SpwanPlayerBrick(_pos);
        }
        else if (rand > 60)
        {
            // npc brick
            SpwanNPCBrick(_pos);
        }
    }

    void SpwanPlayerBrick(Vector3 pos)
    {
        GameObject gg = Instantiate(brick, transform);
        gg.transform.localPosition = pos;
        gg.GetComponent<brick>().SetBrick(true);
    }
    void SpwanNPCBrick(Vector3 pos)
    {
        GameObject gg = Instantiate(brick, transform);
        gg.transform.localPosition = pos;
        gg.GetComponent<brick>().SetBrick(false);
    }
    void SpwanBomb(Vector3 pos)
    {
        GameObject gg = Instantiate(bomb, transform);
        gg.transform.localPosition = pos;
    }
}
