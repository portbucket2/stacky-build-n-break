﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using TMPro;

public class House : MonoBehaviour
{
    [SerializeField] int totalBricks = 24;
    [SerializeField] int activatedTotal = 0;
    [SerializeField] bool addBrick = false;
    

    [Header("Debug")]
    [SerializeField] bool houseComplete = false;
    [SerializeField] TextMeshProUGUI progression;
    [SerializeField] ParticleSystem explosion;
    [SerializeField] ParticleSystem buildingSmoke;
    [SerializeField] List<GameObject> windows;
    [SerializeField] List<GameObject> bricks;

    readonly string HOUSE = "playerHouse";
    readonly string NPCHOUSE = "npcHouse";

    bool windowsOn = false;

    WaitForSeconds WAIT = new WaitForSeconds(0.05f);
    WaitForEndOfFrame ENDOFFRAME = new WaitForEndOfFrame();

    GameController gameController;

    void Start()
    {
        gameController = GameController.GetController();
        bricks = new List<GameObject>();
        int max = totalBricks;
        for (int i = 0; i < max; i++)
        {
            bricks.Add(transform.GetChild(i).gameObject);
        }
        windows = new List<GameObject>();
        for (int i = max; i < transform.childCount; i++)
        {
            windows.Add(transform.GetChild(i).gameObject);
        }
        AllBricksOff();
        UpdateProgression();
    }
    private void Update()
    {
        if (addBrick)
        {
            addBrick = false;
            AddBricks(1);
        }
    }
    public void SetHouse(ParticleSystem _smoke, ParticleSystem _explosion, TextMeshProUGUI _progression)
    {
        buildingSmoke = _smoke;
        explosion = _explosion;
        progression = _progression;
    }
    public void AddBricks(int total)
    {
        if (!houseComplete)
        {
            activatedTotal += total;
            if (activatedTotal >= bricks.Count)
            {
                Debug.LogError("house complete");
                AllWindowsOn();
                activatedTotal = bricks.Count;
                houseComplete = true;
                transform.GetComponentInParent<BoxCollider>().enabled = false;
                if (transform.parent.CompareTag(HOUSE))
                {
                    // player won
                    gameController.LevelComplete();
                }
                else
                {
                    //npc won
                    gameController.LevelFailed();
                }
            }
            UpdateProgression();
            StartCoroutine(SlowBuilding(activatedTotal));
            //if (activatedTotal > 6)
            //{
            //    AllWindowsOn();
            //}
            buildingSmoke.Play();
        }
    }
    IEnumerator SlowBuilding(int max)
    {
        for (int i = 0; i < max; i++)
        {
            bricks[i].SetActive(true);
            yield return WAIT;
        }
    }
    public void AddBombs(int total)
    {
        activatedTotal -= total * 3;

        if (activatedTotal <= 0)
        {
            activatedTotal = 0;
            Debug.LogError("house destroyed");
        }
        UpdateProgression();
        StartCoroutine(SlowBombing(total));
        //if (activatedTotal < 6)
        //{
        //    AllWindowsOff();
        //}
        explosion.Play();
    }
    IEnumerator SlowBombing(int total)
    {
        for (int i = 0; i < totalBricks; i++)
        {
            bricks[i].SetActive(false);
        }

        for (int i = 0; i < activatedTotal; i++)
        {
            bricks[i].SetActive(true);
        }
        for (int i = 0; i < total; i++)
        {
            explosion.Play();
            yield return WAIT;
        }
        
    }
    void AllBricksOff()
    {
        int max = bricks.Count;
        for (int i = 0; i < max; i++)
        {
            bricks[i].SetActive(false);
        }

        max = windows.Count;
        for (int i = 0; i < max; i++)
        {
            windows[i].SetActive(false);
        }
    }

    void AllWindowsOn()
    {
        if (!windowsOn)
        {
            StartCoroutine(WindowsOnRoutine());
        }
        windowsOn = true;
    }
    void AllWindowsOff()
    {
        if (windowsOn)
        {
            int max = windows.Count;
            for (int i = 0; i < max; i++)
            {
                windows[i].SetActive(false);
            }
        }
        windowsOn = false;
    }
    IEnumerator WindowsOnRoutine()
    {
        int max = windows.Count;
        for (int i = 0; i < max; i++)
        {
            windows[i].SetActive(true);
            yield return WAIT;
        }
    }
    void UpdateProgression()
    {
        progression.text = activatedTotal + " / " + totalBricks;
    }
}
