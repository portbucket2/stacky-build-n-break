﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class HouseSpwaner : MonoBehaviour
{
    [SerializeField] int currentLevel = 0;
    [SerializeField] ParticleSystem smoke;
    [SerializeField] ParticleSystem explosion;
    [SerializeField] TextMeshProUGUI progression;
    [SerializeField] List<GameObject> housePrefabs;
    [Header("Debug")]
    [SerializeField] House currentHouse;

    GameManager gameManager;

    void Start()
    {
        gameManager = GameManager.GetManager();
        if (gameManager)
            currentLevel = gameManager.GetlevelCount();
        else
            currentLevel = 0;
        //GameObject gg = Instantiate(housePrefabs[currentLevel], transform);
        GameObject gg = housePrefabs[currentLevel];
        gg.SetActive(true);
        currentHouse = gg.GetComponent<House>();
        currentHouse.SetHouse(smoke, explosion, progression);
    }

    public House GetCurrentHouse()
    {
        return currentHouse;
    }
}
