﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using DG.Tweening;

public class NPC : MonoBehaviour
{
    [SerializeField] Animator animator;
    [SerializeField] GameObject brickPrefab;
    [SerializeField] GameObject bombPrefab;
    [SerializeField] Transform npcBuilding;
    [SerializeField] Transform playerBuilding;
    [Header("AI")]
    [SerializeField] float detectionRadius;

    [Header("Debug")]
    [SerializeField] float walkSpeed = 0;
    [SerializeField] NavMeshAgent agent;
    [SerializeField] Rigidbody rb;
    [SerializeField] bool isMoving = false;
    [SerializeField] bool isBuilding = false;
    [SerializeField] bool isBombing = false;
    [SerializeField] House targetHouse;
    [SerializeField] bool levelEnded = false;

    [SerializeField] int brickCount = 0;
    [SerializeField] int bombCount = 0;
    [SerializeField] int totalBrick = 30;
    [SerializeField] int totalBomb = 30;
    [SerializeField] Vector3 brickStackOffset;
    [SerializeField] Vector3 bombStackOffset;
    [SerializeField] List<GameObject> bricks;
    [SerializeField] List<Vector3> bricksPositions;
    [SerializeField] List<GameObject> bombs;
    [SerializeField] List<Vector3> bombsPositions;

    readonly string HOUSE = "playerHouse";
    readonly string NPCHOUSE = "npcHouse";

    readonly string RED = "redBrick";
    readonly string BLUE = "blueBrick";
    readonly string BOMB = "bomb";
    readonly string SPEED = "speed";

    WaitForEndOfFrame ENDOFFRAME = new WaitForEndOfFrame();
    WaitForSeconds WAITONE = new WaitForSeconds(1f);
    WaitForSeconds WAITTWO = new WaitForSeconds(2f);
    WaitForSeconds WAITHALF = new WaitForSeconds(0.3f);


    private void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
        rb = GetComponent<Rigidbody>();
    }
    void Start()
    {
        BrickInit();
        BombInit();
        
    }
    private void Update()
    {
        if (isMoving)
        {
            walkSpeed = agent.velocity.magnitude;
            if (walkSpeed > 1)
                walkSpeed = 1;
            animator.SetFloat(SPEED, walkSpeed);
        }else{
            animator.SetFloat(SPEED, 0);
        }

    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.CompareTag(NPCHOUSE))
        {
            isBuilding = true;
            isBombing = false;
            targetHouse = other.transform.GetComponent<HouseSpwaner>().GetCurrentHouse();
            StartCoroutine(BuildingRoutine());
        }
        else if (other.transform.CompareTag(HOUSE))
        {
            isBombing = true;
            isBuilding = false;
            targetHouse = other.transform.GetComponent<HouseSpwaner>().GetCurrentHouse();
            StartCoroutine(BombingRoutine());
        }
    }

    public void StopAllActivity()
    {
        StopAllCoroutines();
        agent.SetDestination(transform.position);
        levelEnded = true;
        // cry animation
    }

    //=`-=`-`=-`=-`=-=-=-`=-`=-`=-`=-`=-`=-`=-`=`-Movement=`-=`-=`-=-`=-`=-`=-`=-`=-`=-`=-`=-`=`-=`-=`-=`-=`-=`-`=-`=-`
    public void BehaviorStart() {
        if (!levelEnded)
        {
            StartCoroutine(BehaviorRoutine());
        }
    }
    IEnumerator BehaviorRoutine()
    {
        yield return WAITHALF;
        MoveTo(SearchBrick());
    }
    IEnumerator WaitedBehaviorRoutine()
    {
        yield return WAITTWO;
        MoveTo(SearchBrick());
    }
    void MoveTo(Vector3 target)
    {
        StartCoroutine(MoveToRoutine(target));
    }
    IEnumerator MoveToRoutine(Vector3 target)
    {

        agent.SetDestination(target);
        while (Vector3.Distance(transform.position, target) > 0.1f) 
        {
            isMoving = true;
            yield return ENDOFFRAME;
        }
        isMoving = false;
        agent.SetDestination(transform.position);

        
    }
    private Vector3 SearchBrick()
    {
        Vector3 result = Vector3.zero;
        //List<brick> blues = new List<brick>();
        Collider[] hitColliders = Physics.OverlapSphere(transform.position, detectionRadius);
        if (hitColliders.Length > 0)
        {
            foreach (var hitCollider in hitColliders)
            {
                if (hitCollider.CompareTag(RED))
                {
                    //blues.Add(hitCollider.GetComponent<brick>());
                    result = hitCollider.transform.position;
                    Debug.DrawLine(transform.position, result, Color.blue, 10f);
                    break;
                }
            }
        }
        else
        {
            Debug.Log("no brick detected!!");
            StartCoroutine(WaitedBehaviorRoutine());
        }
        return result;
    }
    private Vector3 SearchBomb()
    {
        List<Bomb> bombs = new List<Bomb>();
        Collider[] hitColliders = Physics.OverlapSphere(transform.position, detectionRadius);
        foreach (var hitCollider in hitColliders)
        {
            if (hitCollider.CompareTag(BOMB))
            {
                bombs.Add(hitCollider.GetComponent<Bomb>());
            }
        }
        Vector3 result = bombs[Random.Range(0, bombs.Count)].transform.position;

        return result;
    }
    void Build()
    {
        StopAllCoroutines();
        MoveTo(npcBuilding.position);
    }
    void Bomb()
    {
        StopAllCoroutines();
        MoveTo(playerBuilding.position);
    }

    //=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-`=`-=`-=`-=`-=`-`=-`=-`=`-=`-=`-interaction-=-=--=-=-=-=-=-=-=--=-=-=-=-=-=-=-=-=-=-

    public void AddBrick()
    {
        brickCount++;
        ShowBrick();
       
        if (brickCount >= 10)
        {
            // build routine
            StopAllCoroutines();
            Build();
        }
        else
        {
            StopAllCoroutines();
            BehaviorStart();
        }
    }
    public void AddBomb()
    {
        bombCount++;
        ShowBomb();

        if (bombCount >= 3)
        {
            // build routine
            StopAllCoroutines();
            Bomb();
        }
        else
        {
            StopAllCoroutines();
            BehaviorStart();
        }
    }
    void ShowBrick()
    {
        int max = bricks.Count;
        for (int i = 0; i < max; i++)
        {
            bricks[i].SetActive(false);
        }
        for (int i = 0; i < brickCount; i++)
        {
            bricks[i].SetActive(true);
        }
    }
    void ShowBomb()
    {
        int max = bombs.Count;
        for (int i = 0; i < max; i++)
        {
            bombs[i].SetActive(false);
        }
        for (int i = 0; i < bombCount; i++)
        {
            bombs[i].SetActive(true);
        }
    }


    void BrickInit()
    {
        bricks = new List<GameObject>();
        bricksPositions = new List<Vector3>();
        float yy = 0.8f;
        //brickStackOffset += new Vector3(0f, yy, 0f);
        GameObject gg;
        for (int i = 0; i < totalBrick; i++)
        {
            brickStackOffset += new Vector3(0f, yy, 0f);
            gg = Instantiate(brickPrefab, transform.position + brickStackOffset, Quaternion.identity, transform);
            //yy += 0.5f;
            gg.GetComponent<BoxCollider>().enabled = false;
            bricks.Add(gg);
            bricksPositions.Add(gg.transform.localPosition);
        }
        for (int i = 0; i < totalBrick; i++)
        {
            bricks[i].SetActive(false);
            bricks[i].GetComponent<BoxCollider>().enabled = false;
        }
    }
    void ResetBricksPosition()
    {
        //Debug.Log("brick reset");
        for (int i = 0; i < totalBrick; i++)
        {
            bricks[i].transform.localPosition = bricksPositions[i];
        }
    }
    void BombInit()
    {
        bombs = new List<GameObject>();
        bombsPositions = new List<Vector3>();
        float yy = 0.8f;
        //brickStackOffset += new Vector3(0f, yy, 0f);
        GameObject gg;
        for (int i = 0; i < totalBrick; i++)
        {
            bombStackOffset += new Vector3(0f, yy, 0f);
            gg = Instantiate(bombPrefab, transform.position + bombStackOffset, Quaternion.identity, transform);
            //yy += 0.5f;
            bombs.Add(gg);
            bombsPositions.Add(gg.transform.localPosition);
        }
        for (int i = 0; i < totalBrick; i++)
        {
            bombs[i].SetActive(false);
            bombs[i].GetComponent<SphereCollider>().enabled = false;
        }
    }
    void ResetBombsPosition()
    {
        //Debug.Log("brick reset");
        for (int i = 0; i < totalBomb; i++)
        {
            bombs[i].transform.localPosition = bombsPositions[i];
        }
    }
    IEnumerator BuildingRoutine()
    {
        if (isBuilding && brickCount > 0)
        {
            BrickSent();
            for (int i = brickCount - 1; i > 0; i--)
            {
                bricks[brickCount - 1].transform.DOMove(targetHouse.transform.position, 0.2f).SetEase(Ease.Flash);
                yield return WAITHALF;
            }
        }
        BehaviorStart();
    }
    void BrickSent()
    {
        targetHouse.AddBricks(brickCount);
        brickCount = 0;
        ShowBrick();

        ResetBricksPosition();
    }
    IEnumerator BombingRoutine()
    {
        if (isBombing && bombCount > 0)
        {
            BombSent();
            for (int i = bombCount - 1; i > 0; i--)
            {
                bombs[bombCount - 1].transform.DOMove(targetHouse.transform.position, 0.2f).SetEase(Ease.Flash);
                yield return WAITHALF;
            }
        }
        BehaviorStart();
    }
    void BombSent()
    {
        targetHouse.AddBombs(bombCount);
        bombCount = 0;
        ShowBomb();

        ResetBombsPosition();
    }
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        //Use the same vars you use to draw your Overlap SPhere to draw your Wire Sphere.
        Gizmos.DrawWireSphere(transform.position, detectionRadius);
    }
}
