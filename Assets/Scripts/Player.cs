﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using DG.Tweening;

public class Player : MonoBehaviour
{
    [SerializeField] bool inputEnabled = true;
    [SerializeField] VariableJoystick joystick;
    [SerializeField] float speed = 1f;
    [SerializeField] GameObject brickPrefab;
    [SerializeField] GameObject bombPrefab;
    [SerializeField] Animator animator;

    [Header("Debug")]
    [SerializeField] float walkSpeed = 0;
    [SerializeField] bool isBuilding = false;
    [SerializeField] bool isBombing = false;
    [SerializeField] House targetHouse;
    [SerializeField] Rigidbody rb;
    [SerializeField] float smoothTime = 0.3F;
    [SerializeField] Vector3 velocity = Vector3.zero;
    [SerializeField] NavMeshAgent agent;
    [SerializeField] int brickCount = 0;
    [SerializeField] int bombCount = 0;
    [SerializeField] int totalBrick = 30;
    [SerializeField] int totalBomb = 30;
    [SerializeField] Vector3 brickStackOffset;
    [SerializeField] Vector3 bombStackOffset;
    [SerializeField] List<GameObject> bricks;
    [SerializeField] List<Vector3> bricksPositions;
    [SerializeField] List<GameObject> bombs;
    [SerializeField] List<Vector3> bombsPositions;

    WaitForSeconds WAIT = new WaitForSeconds(0.05f);
    WaitForEndOfFrame ENDOFFRAME = new WaitForEndOfFrame();

    readonly string HOUSE = "playerHouse";
    readonly string NPCHOUSE = "npcHouse";
    readonly string SPEED = "speed";
    readonly string RUN = "run";
    readonly string VICTORY = "victory";
    readonly string BOMB = "bomb";
    readonly string BRICK = "brick";

    float horizontal;
    float vertical;
    Vector3 nextPos;
    private void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
        rb = GetComponent<Rigidbody>();
    }
    void Start()
    {
        BrickInit();
        BombInit();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButton(0))
        {
            walkSpeed = rb.velocity.magnitude;
            if (walkSpeed != 0)
                animator.SetFloat(SPEED, 1);

            if (inputEnabled)
            {
                float horizontal = joystick.Horizontal * speed;
                float vertical = joystick.Vertical * speed;
                nextPos = transform.position + new Vector3(horizontal, 0f, vertical);
                transform.position = Vector3.SmoothDamp(transform.position, nextPos, ref velocity, smoothTime);
                transform.LookAt(nextPos);
                //agent.SetDestination(nextPos);
            }
        }
        if (Input.GetMouseButtonUp(0))
        {
            animator.SetFloat(SPEED, 0);
        }
        
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.CompareTag(HOUSE))
        {
            isBuilding = true;
            targetHouse = other.transform.GetComponent<HouseSpwaner>().GetCurrentHouse();
            StartCoroutine(BuildingRoutine());
        }
        else if (other.transform.CompareTag(NPCHOUSE))
        {
            isBombing = true;
            targetHouse = other.transform.GetComponent<HouseSpwaner>().GetCurrentHouse();
            StartCoroutine(BombingRoutine());
        }
    }
    private void OnTriggerExit(Collider other)
    {
        isBuilding = false;
        isBombing = false;
    }
    public void InputSwitch(bool isOn) {
        inputEnabled = isOn;
        agent.SetDestination(transform.position);
    }
    public void AddBrick()
    {
        brickCount++;
        ShowBrick();

    }
    public void AddBomb()
    {
        bombCount++;
        ShowBomb();
    }
    public void Victory()
    {
        brickCount = 0;
        bombCount = 0;
        ShowBrick();
        ShowBomb();

        transform.LookAt(-Vector3.forward);
        animator.SetTrigger(VICTORY);
    }
    void ShowBrick()
    {
        int max = bricks.Count;
        for (int i = 0; i < max; i++)
        {
            bricks[i].SetActive(false);
        }
        for (int i = 0; i < brickCount; i++)
        {
            bricks[i].SetActive(true);
        }
    }
    void ShowBomb()
    {
        int max = bombs.Count;
        for (int i = 0; i < max; i++)
        {
            bombs[i].SetActive(false);
        }
        for (int i = 0; i < bombCount; i++)
        {
            bombs[i].SetActive(true);
        }
    }


    void BrickInit()
    {
        bricks = new List<GameObject>();
        bricksPositions = new List<Vector3>();
        float yy = 0.8f;
        //brickStackOffset += new Vector3(0f, yy, 0f);
        GameObject gg;
        for (int i = 0; i < totalBrick; i++)
        {
            brickStackOffset += new Vector3(0f, yy, 0f);
            gg = Instantiate(brickPrefab, transform.position + brickStackOffset, Quaternion.identity, transform);
            //yy += 0.5f;
            gg.GetComponent<BoxCollider>().enabled = false;
            bricks.Add(gg);
            bricksPositions.Add(gg.transform.localPosition);
        }
        for (int i = 0; i < totalBrick; i++)
        {
            bricks[i].SetActive(false);
            bricks[i].GetComponent<BoxCollider>().enabled = false;
        }
    }
    void ResetBricksPosition()
    {
        //Debug.Log("brick reset");
        for (int i = 0; i < totalBrick; i++)
        {
            bricks[i].transform.DOKill();
            bricks[i].transform.localPosition = bricksPositions[i];
        }
    }
    void BombInit()
    {
        bombs = new List<GameObject>();
        bombsPositions = new List<Vector3>();
        float yy = 0.8f;
        //brickStackOffset += new Vector3(0f, yy, 0f);
        GameObject gg;
        for (int i = 0; i < totalBomb; i++)
        {
            bombStackOffset += new Vector3(0f, yy, 0f);
            gg = Instantiate(bombPrefab, transform.position + bombStackOffset, Quaternion.identity, transform);
            //yy += 0.5f;
            bombs.Add(gg);
            bombsPositions.Add(gg.transform.localPosition);
        }
        for (int i = 0; i < totalBrick; i++)
        {
            bombs[i].transform.GetComponent<SphereCollider>().enabled = false;
            bombs[i].SetActive(false);
        }
    }
    void ResetBombsPosition()
    {
        //Debug.Log("brick reset");
        for (int i = 0; i < totalBomb; i++)
        {
            bombs[i].transform.DOKill();
            bombs[i].transform.localPosition = bombsPositions[i];
            bombs[i].GetComponent<SphereCollider>().enabled = false;
        }
    }
    IEnumerator BuildingRoutine()
    {
        if (isBuilding && brickCount > 0) 
        {
            int tempBrick = brickCount;
            BrickSent();
            for (int i = tempBrick - 1; i > 0; i--)
            {
                bricks[i].transform.DOMove(targetHouse.transform.position, 0.2f).SetEase(Ease.Flash);
                animator.SetTrigger(BRICK);
                yield return WAIT;
            }
            yield return ENDOFFRAME;
            ShowBrick();
            yield return ENDOFFRAME;
            ResetBricksPosition();
        }
        
    }
    void BrickSent()
    {
        targetHouse.AddBricks(brickCount);
        brickCount = 0;;
        
        //ResetBricksPosition();
    }
    IEnumerator BombingRoutine()
    {
        if (isBombing && bombCount > 0)
        {
            int tempBomb = bombCount;
            BombSent();
            for (int i = tempBomb - 1; i > 0; i--)
            {
                bombs[i].transform.DOMove(targetHouse.transform.position, 0.2f).SetEase(Ease.Flash);
                animator.SetTrigger(BOMB);
                yield return WAIT;
            }
            yield return ENDOFFRAME;
            ShowBomb();
            yield return ENDOFFRAME;
            ResetBombsPosition();
        }

    }
    void BombSent()
    {
        targetHouse.AddBombs(bombCount);
        bombCount = 0;
        
        //ResetBombsPosition();
    }
}
