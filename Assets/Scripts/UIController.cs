﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;

public class UIController : MonoBehaviour
{
    [SerializeField] GameObject titlePanel;
    [SerializeField] GameObject completedPanel;
    [SerializeField] GameObject failedPanel;
    [SerializeField] Button startButton;
    [SerializeField] Button nextButton;
    [SerializeField] Button retryButton;

    GameController gameController;
    GameManager gameManager;
    public static bool titleSeen;

    void Start()
    {
        gameManager = GameManager.GetManager();
        gameController = GameController.GetController();
        startButton.onClick.AddListener(delegate { LevelStart(); });
        nextButton.onClick.AddListener(delegate { NextLevel(); });
        retryButton.onClick.AddListener(delegate { RetryLevel(); });
        if (!titleSeen)
        {
            titlePanel.SetActive(true);
        }
        else
        {
            LevelStart();
        }
    }

    void LevelStart()
    {
        gameController.LevelStart();
        titlePanel.SetActive(false);
        titleSeen = true;
    }
    public void LevelCompleted()
    {
        completedPanel.SetActive(true);
    }
    public void LevelFailed()
    {
        failedPanel.SetActive(true);
    }
    void NextLevel()
    {
        gameManager.GotoNextStage();
    }
    void RetryLevel()
    {
        gameManager.ResetStage();
    }

}
