﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class brick : MonoBehaviour
{
    [SerializeField] bool isForPayer = true;
    [SerializeField] MeshRenderer brickMesh;
    [Header("Debug")]
    [SerializeField] Material playerBrick;
    [SerializeField] Material npcBrick;
    [SerializeField] GroundSpwaner spwaner;

    readonly string PLAYER = "Player";
    readonly string NPC = "npc";
    readonly string RED = "redBrick";
    readonly string BLUE = "blueBrick";
    WaitForSeconds WAIT = new WaitForSeconds(1.5f);

    private void Awake()
    {
        spwaner = GetComponentInParent<GroundSpwaner>();
    }

    void Start()
    {

    }
    public void SetBrick(bool _isForPlayer)
    {
        isForPayer = _isForPlayer;
        if (isForPayer)
        {
            brickMesh.material = playerBrick;
            transform.tag = BLUE;
        }
        else
        {
            brickMesh.material = npcBrick;
            transform.tag = RED;
        }

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(PLAYER) && isForPayer)
        {
            other.GetComponent<Player>().AddBrick();
            ReSpwan();
        }
        else if(other.CompareTag(NPC) && !isForPayer)
        {
            other.GetComponent<NPC>().AddBrick();
            ReSpwan();
        }
        else
        {

        }
    }

    void ReSpwan()
    {
        StartCoroutine(ReSpwanRoutine());
    }
    IEnumerator ReSpwanRoutine()
    {
        GetComponent<BoxCollider>().enabled = false;
        transform.GetChild(0).gameObject.SetActive(false);
        yield return WAIT;
        spwaner.RandomSpwan(transform.position);
        gameObject.SetActive(false);
    }
}
